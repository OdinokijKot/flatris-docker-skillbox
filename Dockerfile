FROM node:12

RUN apt-get update && apt-get install -y nano

RUN mkdir /skillbox
COPY package.json /skillbox
WORKDIR /skillbox

RUN yarn install

COPY . /skillbox

RUN yarn build

CMD yarn start

EXPOSE 3000